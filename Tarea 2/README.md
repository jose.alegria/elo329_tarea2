#### Approach: 
Esta tarea busca proyectar el comportamiento de una pandemia en un espacio confinado con múltiples variables a considerar. Los archivos que la componen son:

- makefile.txt
- configurationFile.txt
- Comuna.java
- ComunaView.java
- Controller.java
- Pedestrian.java
- PedestrianView.java
- Simulator.java
- SimulatorConfig.java
- SimulatorMenuBar.java
- Vacunatorio.java
- VacunatorioView.java
- Stage4.java

#### Instrucciones de compilación:
Primero descargar archivos de cada Stage por separado, asegurarse de que se mantenga cada stage en su respectivo directorio para evitar confusión de archivos por nombres similares.
Una vez que cada carpeta esté descargada, continuar con la ejecución etapa por etapa, para esto le damos uso al archivo makefile, el cual requiere una modificación por parte del ayudante que revisará.
En el makefile se hace uso de una variable DEFAULT, esta variable se rellena con el path a la carpeta lib del sdk del ayudante.
Estando parado en la carpeta del stage que se desea compilar, se utilizan los siguientes comandos (en Linux y teniendo make descargado):

- make (genera archivos .class)
- make run (ejecuta el codigo)
- make clean (elimina archivos .class)

En caso que hay error InputMismatchException, cambiar el separador decimal de "." a "," o vice versa.
Durante la realización de la tarea decidimos no optar por el crédito extra ofrecido.

### Instrucciones Uso Menu:
- Para todos los botones aparte de los TextFields, actualizan las variables una vez que se hace click en ellos.
- Tener precaucion con los TextFields, se debe escribir un double y luego apretar enter para que el cambio se realiza para la proxima simulacion.
