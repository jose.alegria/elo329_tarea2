import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.layout.Pane;

public class Comuna {
    private Pedestrian person;
    private Rectangle2D territory;
    private ComunaView view;
    private Pane graph;

    public Comuna(){
        double width = SimulatorConfig.WIDTH;
        double length = SimulatorConfig.LENGTH;
        territory = new Rectangle2D(0,0, width, length);
        double speed = SimulatorConfig.SPEED;
        double deltaAngle = SimulatorConfig.DELTA_THETA;
        view = new ComunaView(this);
        graph = new Pane();  // to be completed in other stages.
    }

    //getters
    public double getWidth() {
        return territory.getWidth();
    }
    public double getHeight() {
        return territory.getHeight();
    }


    public void computeNextState (double delta_t/*, double d, double p0, double tiempoInfeccion, double p1, double p2*/) {
        person.computeNextState(delta_t);
    }
    public void updateState () {
        person.updateState();
    }
    public void setPerson(Pedestrian person){
        this.person = person;
        double rx = Math.random()*getWidth();
        double ry = Math.random()*getHeight();
        //llamar x, y en person y usarlos
        person.updateStateRandom(rx, ry);
    } //Asigna la persona a la comuna y la coloca en una coordenada x-y aleatoria dentro de los rangos permitidos.

    public void updateView(){
        view.update();
    }
    public Pedestrian getPedestrian() {
        return person;
    }
    public ComunaView getView() {
        return view;
    }
    public Pane getGraph(){
        return graph;
    }
}
