import java.awt.geom.Point2D;

public class Pedestrian {
    private double x, y, speed, angle, deltaAngle;
    private Point2D ubicacion;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private PedestrianView view;

    // Constructor
    public Pedestrian(Comuna comuna) {
        x = 0;
        y = 0;
        angle = Math.random() * 2 * Math.PI; // Le asigna un angulo de direccion aleatorio a la persona entre 0-2PI rad.
        this.comuna = comuna;
        this.speed = SimulatorConfig.SPEED;
        this.deltaAngle = SimulatorConfig.DELTA_THETA;
        ubicacion = new Point2D.Double(x,y);
        view = new PedestrianView(comuna, this);
    }

    // Get Methods
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getX_tPlusDelta() {
        return x_tPlusDelta;
    }
    public double getY_tPlusDelta() {
        return y_tPlusDelta;
    }
    public static String getStateDescription() {
        return "Coordenadas x-y";
    }
    public PedestrianView getPedestrianView(){
        return view;
    }

    // State Methods
    public void computeAngle(){
        double r = Math.random(); // r entre 0 y 1
        double r2 = Math.random(); // r entre 0 y 1 para ver naturaleza del cambio de angulo
        if (r2 >= 0.5) {
            angle += r * deltaAngle; // cambia el angulo con delta positivo
        } else {
            angle -= r * deltaAngle; // cambia el angulo con delta negativo
        }
    } //Calculo del prox angulo en delta_t
    public void computeXY(double delta_t){
        double time;
        double time2;
        double angle2 = angle;

        if ((x + speed * delta_t * Math.cos(angle)) < 0) {
            time = x / (speed * (Math.abs(Math.cos(angle))));
            time2 = delta_t - time;
            x_tPlusDelta = x + speed * time * (Math.cos(angle));
            angle = (3 * Math.PI) - angle;
            x_tPlusDelta = x_tPlusDelta + speed * time2 * (Math.cos(angle));
        } else if ((x + speed * delta_t * Math.cos(angle)) > comuna.getWidth()) {
            time = (comuna.getWidth() - x) / (speed * (Math.abs(Math.cos(angle))));
            time2 = delta_t - time;
            x_tPlusDelta = x + speed * time * (Math.cos(angle));
            angle = (3 * Math.PI) - angle;
            x_tPlusDelta = x_tPlusDelta + speed * time2 * (Math.cos(angle));
        } else {
            x_tPlusDelta = x + speed * delta_t * Math.cos(angle);
        }

        if ((y + speed * delta_t * Math.sin(angle2)) < comuna.getHeight()
                && (y + speed * delta_t * Math.sin(angle2)) > 0) {
            y_tPlusDelta = y + speed * delta_t * Math.sin(angle2);

        } else if (y + speed * delta_t * Math.sin(angle2) > comuna.getHeight()) {

            time = (comuna.getHeight() - y) / (speed * (Math.abs(Math.sin(angle2))));
            time2 = delta_t - time;
            y_tPlusDelta = y + speed * time * Math.sin(angle2);
            angle2 = (2 * Math.PI) - angle2;
            y_tPlusDelta = y_tPlusDelta + (speed * time2 * (Math.sin(angle2)));
            angle = angle2;

        } else if (y + speed * delta_t * Math.sin(angle2) < 0) {
            time = y / (speed * (Math.abs(Math.sin(angle2))));
            time2 = delta_t - time;
            y_tPlusDelta = y + speed * time * Math.sin(angle2);
            angle2 = (2 * Math.PI) - angle2;
            y_tPlusDelta = y_tPlusDelta + (speed * time2 * ((Math.sin(angle2))));
            angle = angle2;
        }
    } //Calculo de x_tPlusDelta y y_tPlusDelta
    public void computeNextState(double delta_t) {
        computeAngle(); // aumenta x e y tomando el angulo nuevo y el tiempo de mov.
        computeXY(delta_t); // si la persona sale de la comuna rebota
    } //Se preocupa de actulizar variasbles como estadoSalud_tPlusDelta, x_tPlusdeta y y_tPlusdeta.

    //Update Methods
    public void updateState() {
        x = x_tPlusDelta;
        y = y_tPlusDelta;
        ubicacion.setLocation(x_tPlusDelta, y_tPlusDelta);
    } //Va actualizando posiciones
    public void updateStateRandom(double x, double y) {
        this.x = x;
        this.y = y;
    } //Solo es utilizado cuando la comuna busca colocar al individuo en un lugar random de su territorio.
    public void updateView(){
        view.update();
    }
}

