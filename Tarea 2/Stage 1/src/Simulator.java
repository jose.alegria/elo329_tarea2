import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;

public class Simulator {
    private Timeline animation;
    private Comuna comuna;
    private double simulationSamplingTime;
    private double simulationTime;  // acompaña el tiempo real, más rapido o más lento.
    private double delta_t;   // precision del tiempo de la simulacion.

    /**
     * @param framePerSecond frequency of new views on screen
     * @param simulationTime2realTimeRate how faster the simulation runs relative to real time
     * @param comuna la comuna :)
     */
    public Simulator (double framePerSecond, double simulationTime2realTimeRate, Comuna comuna){
        this.comuna = comuna;
        double viewRefreshPeriod = 1 / framePerSecond; // in [ms] real time used to display
        // a new view on application
        simulationSamplingTime = viewRefreshPeriod * simulationTime2realTimeRate;
        delta_t = SimulatorConfig.DELTA_T;
        simulationTime = 0;
        animation = new Timeline(new KeyFrame(Duration.millis(viewRefreshPeriod*1000), e->takeAction()));
        animation.setCycleCount(Timeline.INDEFINITE);
    }
    private void takeAction() {
        double nextStop=simulationTime+simulationSamplingTime;
        for(; simulationTime<nextStop; simulationTime+=delta_t) {
            comuna.computeNextState(delta_t); // compute its next state based on current global state
            comuna.updateState();            // update its state
        }
            comuna.updateView();
        }
    public void start(){
	comuna.getView().getChildren().clear();
	comuna.getView().territoryRefresh();
        animation.play();
	comuna.setPerson(new Pedestrian(comuna));
        comuna.getView().setOnKeyPressed( e->keyHandle(e));
    }
    private void keyHandle (KeyEvent e) {
        /// ?????
    }
    public void stop(){
        animation.stop();
        //?????
    }
    public void speedup(){
        //on key.right
        //simulationSampling *= 2
        //????
    }
    public void slowdown(){
        //on key.right
        //simulationSampling *= 2
        // ???
    }
}
