
import javafx.geometry.Rectangle2D;
import javafx.scene.layout.Pane;

import java.util.ArrayList;

public class Comuna {
    private ArrayList<Pedestrian> people;
    private Rectangle2D territory;
    private ComunaView view;
    private Pane graph;
    private double NumberOfPeople;
    private double NumberOfInfected;

    public Comuna(){
        double width = SimulatorConfig.WIDTH;
        double length = SimulatorConfig.LENGTH;
        territory = new Rectangle2D(0,0, width, length);
        double speed = SimulatorConfig.SPEED;
        double deltaAngle = SimulatorConfig.DELTA_THETA;
        view = new ComunaView(this);
        graph = new Pane();  // to be completed in other stages.
        people = new ArrayList<Pedestrian>();
        NumberOfPeople = SimulatorConfig.N;
        NumberOfInfected = SimulatorConfig.I;

    }

    //getters
    public double getWidth() {
        return territory.getWidth();
    }
    public double getHeight() {
        return territory.getHeight();
    }


    public void computeNextState (double delta_t, double d, double p0, double tiempoInfeccion/*, double p1, double p2*/) {
        for(int i=0; i<people.size(); i++){
            people.get(i).computeNextState(delta_t,d, p0, tiempoInfeccion);
        }
    }
    public void updateState () {
        for(int i=0; i<people.size(); i++){
            people.get(i).updateState();
        }
    }
    public void setPerson(Pedestrian person){
        people.add(person);
        double rx = Math.random()*getWidth();
        double ry = Math.random()*getHeight();
        //llamar x, y en person y usarlos
        person.updateStateRandom(rx, ry);
    } //Asigna la persona a la comuna y la coloca en una coordenada x-y aleatoria dentro de los rangos permitidos.

    public void setPeople(double speed, double deltaAngle){
        if(!(people.isEmpty())){
            clearPastViews();
        }
        people = new ArrayList<Pedestrian>();
        for(int i=0; i<NumberOfPeople;i++){
            if (i<NumberOfInfected){
                Pedestrian person = new Pedestrian(this, speed, deltaAngle, "I");
                this.setPerson(person);
            }else{
                Pedestrian person = new Pedestrian(this, speed, deltaAngle, "S");
                this.setPerson(person);
            }
        }
    }//Se utiliza para poner gente en el territorio con nuevas variables al utilizar play
    public void updateView(){
        view.update();
    }
    public void clearPastViews(){
        view.eraseViews();
    }
    public ArrayList<Pedestrian> getPeople(){
        return people;}
    public ComunaView getView() {
        return view;
    }
    public void setNumberOfPeople(double np){
        NumberOfPeople = np;
    }
    public void setNumberOfInfected(double ni){
        NumberOfInfected = ni;
    }
    public Pane getGraph(){
        return graph;
    }
}
