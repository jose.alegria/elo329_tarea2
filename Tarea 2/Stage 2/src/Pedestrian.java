

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.awt.geom.Point2D;
import java.util.ArrayList;

public class Pedestrian {
    private double x, y, speed, angle, deltaAngle;
    private Point2D ubicacion;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private PedestrianView view;
    private String estadoSalud;
    private String estadoSalud_tPlusDelta;
    private double timerRecuperacion;
    private MediaPlayer mediaplayer;
    private Media media;

    // Constructor
    public Pedestrian(Comuna comuna, double speed, double deltaAngle, String salud) {
        x = 0;
        y = 0;
        angle = Math.random() * 2 * Math.PI; // Le asigna un angulo de direccion aleatorio a la persona entre 0-2PI rad.
        this.comuna = comuna;
        this.speed = speed;
        this.deltaAngle = deltaAngle;
        ubicacion = new Point2D.Double(x,y);
        estadoSalud = salud;
        estadoSalud_tPlusDelta = salud;
        timerRecuperacion = 0;
        media = new Media("https://www.myinstants.com/media/sounds/movie_1.mp3");
        mediaplayer = new MediaPlayer(media);
    }

    // Get Methods
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getX_tPlusDelta() {
        return x_tPlusDelta;
    }
    public double getY_tPlusDelta() {
        return y_tPlusDelta;
    }
    public static String getStateDescription() {
        return "Coordenadas x-y";
    }
    public PedestrianView getPedestrianView(){
        return view;
    }
    public String getState(){
        return estadoSalud;
    }

    // State Methods
    public void computeAngle(){
        double r = Math.random(); // r entre 0 y 1
        double r2 = Math.random(); // r entre 0 y 1 para ver naturaleza del cambio de angulo
        if (r2 >= 0.5) {
            angle += r * deltaAngle; // cambia el angulo con delta positivo
        } else {
            angle -= r * deltaAngle; // cambia el angulo con delta negativo
        }
    } //Calculo del prox angulo en delta_t
    public void computeXY(double delta_t){
        double time;
        double time2;
        double angle2 = angle;

        if ((x + speed * delta_t * Math.cos(angle)) < 0) {
            time = x / (speed * (Math.abs(Math.cos(angle))));
            time2 = delta_t - time;
            x_tPlusDelta = x + speed * time * (Math.cos(angle));
            angle = (3 * Math.PI) - angle;
            x_tPlusDelta = x_tPlusDelta + speed * time2 * (Math.cos(angle));
        } else if ((x + speed * delta_t * Math.cos(angle)) > comuna.getWidth()) {
            time = (comuna.getWidth() - x) / (speed * (Math.abs(Math.cos(angle))));
            time2 = delta_t - time;
            x_tPlusDelta = x + speed * time * (Math.cos(angle));
            angle = (3 * Math.PI) - angle;
            x_tPlusDelta = x_tPlusDelta + speed * time2 * (Math.cos(angle));
        } else {
            x_tPlusDelta = x + speed * delta_t * Math.cos(angle);
        }

        if ((y + speed * delta_t * Math.sin(angle2)) < comuna.getHeight()
                && (y + speed * delta_t * Math.sin(angle2)) > 0) {
            y_tPlusDelta = y + speed * delta_t * Math.sin(angle2);

        } else if (y + speed * delta_t * Math.sin(angle2) > comuna.getHeight()) {

            time = (comuna.getHeight() - y) / (speed * (Math.abs(Math.sin(angle2))));
            time2 = delta_t - time;
            y_tPlusDelta = y + speed * time * Math.sin(angle2);
            angle2 = (2 * Math.PI) - angle2;
            y_tPlusDelta = y_tPlusDelta + (speed * time2 * (Math.sin(angle2)));
            angle = angle2;

        } else if (y + speed * delta_t * Math.sin(angle2) < 0) {
            time = y / (speed * (Math.abs(Math.sin(angle2))));
            time2 = delta_t - time;
            y_tPlusDelta = y + speed * time * Math.sin(angle2);
            angle2 = (2 * Math.PI) - angle2;
            y_tPlusDelta = y_tPlusDelta + (speed * time2 * ((Math.sin(angle2))));
            angle = angle2;
        }
    } //Calculo de x_tPlusDelta y y_tPlusDelta
    public void computeNextState(double delta_t,double d, double p0, double tiempoInfeccion) {
        if (estadoSalud == "S"){
            LookforPersonInf(comuna.getPeople(), d, p0);
        }
        else if (estadoSalud =="I") {
            startTimerRecovery(delta_t, tiempoInfeccion);
        }
        computeAngle();
        computeXY(delta_t);
        }
    //Se preocupa de actualizar variables como estadoSalud_tPlusDelta, x_tPlusdeta y y_tPlusdeta.
    public void LookforPersonInf(ArrayList<Pedestrian> close_person, double d, double p0){
        for(int i=0; i< close_person.size(); i++){
            double D = Math.sqrt((x-(close_person.get(i).getX()))*(x-(close_person.get(i).getX())) + (y-(close_person.get(i).getY()))*(y-(close_person.get(i).getY()))); //Si es que ciertas condiciones se cumplen entonces hay una probabilidad de que este se infecte.
            if (D <= d && estadoSalud == "S" && close_person.get(i).getState() == "I"){
                double r = Math.random();
                if (r <= p0) {
                    mediaplayer.play();
                    estadoSalud_tPlusDelta = "I";
                } // For es utilizado para ir viendo la distancia del individuo a cada uno de los otros en el array de la comuna.
            }
        }//Calcula la distancia entre el individuo y cada uno de los otros que se encuentran en la comuna, y una vez que encuentra, uno lo suficientemente cerca, revisa  segun las probabilidades si es que se infectara.
    }
    public void startTimerRecovery(double delta_t, double tiempoInfeccion){
        timerRecuperacion += delta_t;
        if(timerRecuperacion>=tiempoInfeccion){
            estadoSalud_tPlusDelta = "R";
            timerRecuperacion = 0;
        } //Ocupa su propio timer para ver si suficiente tiempo ocurrio para que el nuevo estadoSalud sea "R")
    }

    //Update Methods
    public void updateState() {
        x = x_tPlusDelta;
        y = y_tPlusDelta;
        ubicacion.setLocation(x_tPlusDelta, y_tPlusDelta);
        estadoSalud = estadoSalud_tPlusDelta;
    } //Va actualizando posiciones
    public void updateStateRandom(double x, double y) {
        this.x = x;
        this.y = y;
        view = new PedestrianView(comuna,this,estadoSalud);
    } //Solo es utilizado cuando la comuna busca colocar al individuo en un lugar random de su territorio.
    public void updateView(){
        view.update();
    }
    public void eraseView(){
        view.clearView();
    }
}

