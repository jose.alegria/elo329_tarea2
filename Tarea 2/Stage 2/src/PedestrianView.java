

import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;

public class PedestrianView {
    private Pedestrian person;
    private Rectangle viewR;
    private Circle viewC;
    private Polygon viewT;

    private final double SIZE = 5;

    public PedestrianView(Comuna c, Pedestrian p, String salud) {
        person = p;
        double x = person.getX();
        double y = person.getY();
        if (salud == "S") {
            viewT = new Polygon(x-SIZE, y-SIZE, x+SIZE, y-SIZE, x, y + SIZE);
            viewT.setFill(Color.TRANSPARENT);
            viewC = new Circle(x,y,SIZE,Color.TRANSPARENT);
            viewR = new Rectangle(SIZE*2, SIZE*2, Color.BLUE);
            viewR.setX(x - SIZE / 2);   // Rectangle x position is the X coordinate of the
            // upper-left corner of the rectangle
            viewR.setY(y - SIZE / 2); // Rectangle y position is the Y coordinate of the
            // upper-left corner of the rectangle
            c.getView().getChildren().addAll(viewT,viewR,viewC);
        } else if(salud == "I"){
            viewC = new Circle(x, y, SIZE, Color.RED);
            viewR = new Rectangle(SIZE*2, SIZE*2, Color.TRANSPARENT);
            viewT = new Polygon(x-SIZE, y-SIZE, x+SIZE, y-SIZE, x, y + SIZE);
            viewT.setFill(Color.TRANSPARENT);
            viewR.setX(x - SIZE / 2);   // Rectangle x position is the X coordinate of the
            // upper-left corner of the rectangle
            viewR.setY(y - SIZE / 2); // Rectangle y position is the Y coordinate of the
            // upper-left corner of the rectangle
            c.getView().getChildren().addAll(viewT,viewR,viewC);
        }
    }

    public void clearView(){
        viewC.setFill(Color.WHITE);
        viewR.setFill(Color.WHITE);
        viewT.setFill(Color.WHITE);
        viewC.setRadius(0);
        viewR.setWidth(0);
        viewR.setHeight(0);
    }
    public void update() {
        double x = person.getX();
        double y = person.getY();
        viewR.setX(person.getX() - SIZE / 2);
        viewR.setY(person.getY() - SIZE / 2);
        viewC.setCenterX(person.getX());
        viewC.setCenterY(person.getY());
        viewT.getPoints().setAll(x-SIZE, y-SIZE, x+SIZE, y-SIZE, x, y + SIZE);
        if (person.getState() == "I") {
            viewR.setFill(Color.TRANSPARENT);
            viewC.setFill(Color.RED);
        }else if (person.getState() == "R"){
            viewR.setFill(Color.TRANSPARENT);
            viewC.setFill(Color.TRANSPARENT);
            viewT.setFill(Color.GREEN);
        }
    }
}