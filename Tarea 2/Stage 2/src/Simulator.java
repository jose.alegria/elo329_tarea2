

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.input.KeyCode;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;

import java.sql.Time;

public class Simulator {
    private Timeline animation;
    private Comuna comuna;
    private double simulationSamplingTime;
    private double simulationTime;  // acompaña el tiempo real, más rapido o más lento.
    private double speed;
    private double delta_t;   // precision del tiempo de la simulacion.
    private double delta_theta;
    private double dist;
    private double masks;
    private double prob0;
    private double prob1;
    private double prob2;
    private double tInfeccion;
    private double viewRefreshPeriod;
    /**
     * @param framePerSecond frequency of new views on screen
     * @param simulationTime2realTimeRate how faster the simulation runs relative to real time
     * @param comuna la comuna :)
     */
    public Simulator (double framePerSecond, double simulationTime2realTimeRate, Comuna comuna){
        this.comuna = comuna;
        double viewRefreshPeriod = 1 / framePerSecond; // in [ms] real time used to display
        // a new view on application
        simulationSamplingTime = viewRefreshPeriod * simulationTime2realTimeRate;
        delta_t = SimulatorConfig.DELTA_T;
        speed = SimulatorConfig.SPEED;
        delta_theta = SimulatorConfig.DELTA_THETA;
        dist = SimulatorConfig.D;
        masks = SimulatorConfig.M;
        prob0 = SimulatorConfig.P0;
        prob1 = SimulatorConfig.P1;
        prob2 = SimulatorConfig.P2;
        tInfeccion = SimulatorConfig.I_TIME;
        simulationTime = 0;
        animation = new Timeline(new KeyFrame(Duration.millis(viewRefreshPeriod*1000), e->takeAction()));
        animation.setCycleCount(Timeline.INDEFINITE);
    }
    private void takeAction() {
        double nextStop=simulationTime+simulationSamplingTime;
        for(; simulationTime<nextStop; simulationTime+=delta_t) {
            comuna.computeNextState(delta_t,dist,prob0,tInfeccion); // compute its next state based on current global state
            comuna.updateState();
            comuna.updateView();// update its state
        }
    }
    public void start(){
        comuna.setPeople(speed,delta_theta);
        animation.playFromStart();
        comuna.getView().setOnKeyPressed( e->keyHandle(e));
    }
    private void keyHandle (KeyEvent e) {
        if (e.getCode() == KeyCode.RIGHT){
            simulationSamplingTime *=2; //Esto aumenta la velocidad de simulacion en 2
        }else if(e.getCode() == KeyCode.LEFT){
            simulationSamplingTime *=0.5; //Esto disminuye la velocidad de la simulacion a la mitad.
        }
        /// ?????
    }
    public void stop(){
        animation.stop();
        //?????
    }
    public void setSpeed(double s){
        speed = s;
    }
    public void setDelta_t(double dt){
        delta_t = dt;
    }
    public void setDelta_theta(double dt){
        delta_theta = dt;
    }
    public void setDist(double d){
        dist = d;
    }
    public void setMasks(double m){
        masks = m;
    }
    public void setProb0(double p0){
        prob0 = p0;
    }
    public void setProb1(double p1){
        prob1 = p1;
    }
    public void setProb2(double p2){
        prob2 = p2;
    }
    public void settInfeccion(double ti){
        tInfeccion = ti;
    }
    public Comuna getComuna(){
        return comuna;
    }
    public void speedup(){
        //on key.right
        //simulationSampling *= 2
        //????
    }
    public void slowdown(){
        //on key.right
        //simulationSampling *= 2
        // ???
    }
}
