
import javafx.scene.control.*;
import org.w3c.dom.Text;

import java.lang.module.Configuration;

public class SimulatorMenuBar extends MenuBar {
    SimulatorMenuBar (Simulator simulator){
        Menu controlMenu = new Menu("Control");
        getMenus().add(controlMenu);
        Menu settingsMenu = new Menu("Settings");
        getMenus().add(settingsMenu);
        MenuItem start = new MenuItem("Start");
        MenuItem stop = new MenuItem("Stop");
        controlMenu.getItems().addAll(start, stop);
        start.setOnAction(e->{simulator.start();
            settingsMenu.setDisable(true);});
        stop.setOnAction(e->{simulator.stop();
            settingsMenu.setDisable(false);});
        Slider M = new Slider(0, 1, SimulatorConfig.M);
        M.setOnScroll(e->simulator.setMasks(M.getValue()));
        M.setShowTickMarks(true);
        M.setShowTickLabels(true);
        M.setMajorTickUnit(0.1);
        Spinner<Double> N = new Spinner(1, 100, SimulatorConfig.N, 1);
        N.setOnMouseClicked(e->simulator.getComuna().setNumberOfPeople(Double.valueOf(N.getValue())));
        Spinner<Double> I = new Spinner(0,SimulatorConfig.N, SimulatorConfig.I, 1);
        I.setOnMouseClicked(e->simulator.getComuna().setNumberOfInfected(I.getValue()));
        I.setEditable(true);
        MenuItem Masks = new MenuItem("M",M);
        MenuItem TotalN = new MenuItem("N",N);
        MenuItem Infected = new MenuItem("I",I);
        Menu Prob = new Menu("P");
        TextField p0 = new TextField(String.valueOf(SimulatorConfig.P0));
        p0.setOnAction(e->simulator.setProb0(Double.valueOf(p0.getText())));
        TextField p1 = new TextField(String.valueOf(SimulatorConfig.P1));
        p1.setOnAction(e->simulator.setProb1(Double.valueOf(p1.getText())));
        TextField p2 = new TextField(String.valueOf(SimulatorConfig.P2));
        p2.setOnAction(e->simulator.setProb2(Double.valueOf(p2.getText())));
        p0.setPrefColumnCount(2);
        p1.setPrefColumnCount(2);
        p2.setPrefColumnCount(2);
        MenuItem Prob0 = new MenuItem("P0", p0);
        MenuItem Prob1 = new MenuItem("P1", p1);
        MenuItem Prob2 = new MenuItem("P2", p2);
        Prob.getItems().addAll(Prob0, Prob1, Prob2);
        TextField d = new TextField(String.valueOf(SimulatorConfig.D));
        d.setOnAction(e->simulator.setDist(Double.valueOf(d.getText())));
        TextField s = new TextField(String.valueOf(SimulatorConfig.SPEED));
        s.setOnAction(e->simulator.setSpeed(Double.valueOf(s.getText())));
        TextField d_t = new TextField(String.valueOf(SimulatorConfig.DELTA_T));
        d_t.setOnAction(e->simulator.setDelta_t(Double.valueOf(d_t.getText())));
        TextField d_theta = new TextField(String.valueOf(SimulatorConfig.DELTA_THETA));
        d_theta.setOnAction(e->simulator.setDelta_theta(Double.valueOf(d_theta.getText())));
        d.setPrefColumnCount(2);
        MenuItem speed = new MenuItem("Speed", s);
        MenuItem delta_time = new MenuItem("Delta T", d_t);
        MenuItem delta_angle = new MenuItem("Delta Angle", d_theta);
        MenuItem Dist = new MenuItem("D",d);
        settingsMenu.getItems().addAll(Masks,TotalN,Infected,Prob,Dist,speed, delta_time, delta_angle);
        //????????????
    }
}
