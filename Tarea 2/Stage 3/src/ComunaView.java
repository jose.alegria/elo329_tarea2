
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ComunaView extends Group {
    private final Comuna comuna;

    public ComunaView(Comuna c) {
        comuna = c;
        Rectangle territoryView = new Rectangle(comuna.getWidth(), comuna.getHeight(), Color.WHITE);
        territoryView.setStroke(Color.BROWN);
        getChildren().add(territoryView);
        setFocusTraversable(true);  // needed to receive mouse and keyboard events.
    }
    public void eraseViews(){
        for (int i=0; i<comuna.getPeople().size();i++){
            comuna.getPeople().get(i).eraseView(); //Llama los metodos para borrar las vistas de cada persona en el arreglo de comuna.
        }
    }
    public void update() {
        for (int i = 0; i < comuna.getPeople().size(); i++) {
            comuna.getPeople().get(i).updateView(); //LLama los metodos para actualizar las vistas de cada persona en el arreglo de comuna.
        }
    }
}