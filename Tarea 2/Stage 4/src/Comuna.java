import javafx.geometry.Rectangle2D;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import java.util.ArrayList;
public class Comuna {
    private ArrayList<Pedestrian> people;
    static private ArrayList<Vacunatorio> vacunatorios;
    private Rectangle2D territory;
    private ComunaView view; //Dentro de ComunaView tenemos los distintos atributos para poder presentar de manera visual lo que se encuentra dentro de la comuna
    private double NumberOfPeople;
    private double NumberOfInfected;
    private double NumberOfVacs;
    private double SizeOfVacs;
    private double S, I, R, V;
    private XYChart graph; //Los 4 atributos (linea 16-19) son utilizados para construir el grafico y poder modificarlo a traves de la comuna.
    private XYChart.Series sS, sI, sR, sV;//seriesSusceptibles, seriesInfectados, seriesRecuperados.
    private NumberAxis timeAxis; //time transcurrido con el grupo de peatones en la simulacion.
    private NumberAxis countAxis; //Cantidad de personas en la comuna.

    public Comuna(){
        double width = SimulatorConfig.WIDTH;
        double length = SimulatorConfig.LENGTH;
        territory = new Rectangle2D(0,0, width, length);
        view = new ComunaView(this);
        people = new ArrayList<Pedestrian>();
        vacunatorios = new ArrayList<Vacunatorio>();
        NumberOfPeople = SimulatorConfig.N;
        NumberOfInfected = SimulatorConfig.I;
        NumberOfVacs = SimulatorConfig.NUM_VAC;
        SizeOfVacs = SimulatorConfig.VAC_SIZE;
    }

    //Getters methods
    public double getWidth() {
        return territory.getWidth();
    }
    public double getHeight() {
        return territory.getHeight();
    }
    public ArrayList<Pedestrian> getPeople(){
        return people;}
    public ComunaView getView() {
        return view;
    }

    //State methods
    public void computeNextState (double delta_t, double d, double p0, double p1, double p2, double tiempoInfeccion) {
        for(int i=0; i<people.size(); i++){
            people.get(i).computeNextState(delta_t,d, p0, p1, p2, tiempoInfeccion);
        }
    }
    public void updateState () {
        for(int i=0; i<people.size(); i++){
            people.get(i).updateState();
        }
    }
    public void updateHealthState() {
        I = 0;
        S = 0;
        R = 0;
        V = 0;// Se crean contadores para luego mostrar por pantalla cantidad de Inf, Rec,
        // Sus.
        int N = people.size();
        for (int i = 0; i < N; i++) {
            if (people.get(i).getState() == "I") {
                I += 1;
            } else if ((people.get(i).getState()) == "S") {
                S += 1;
            } else if (people.get(i).getState() == "R") {
                R += 1;
            } else if (people.get(i).getState() == "V"){
                V += 1;
            }
        }
    }
    public void updateView(double simtime){ //Agrega puntos a las series mientras el tiempo pasa en la simulacion (en takeaction())
        view.update();
        sS.getData().add(new XYChart.Data(simtime,S));
        sI.getData().add(new XYChart.Data(simtime,I));
        sR.getData().add(new XYChart.Data(simtime,R));
        sV.getData().add(new XYChart.Data(simtime,V));
        if (simtime<1){
            countAxis.setUpperBound(people.size()); //Se asegura que el axis Y del grafico este actualizado con el numero de personas correspondiente de la simulacion.
        }
        if (simtime>100){ //Una vez que el tiempo de simulacion pasa los 100 segundos, el axis X se va reacomodando para que el grafico pueda seguir mostrando la informacion debida.
            timeAxis.setLowerBound(simtime-100);
            timeAxis.setUpperBound(simtime+100);
        }
    }
    public void clearPastViews(){
        view.eraseViews(); //Llama los metodos correspondientes para borrar las vistas de los peatones.
        graph.getData().removeAll(sS,sI,sR,sV); //Se asegura de remover las series del grafico para que quede en blanco para el proximo grupo.
    }

    //Setters methods
    public void setPerson(Pedestrian person){
        people.add(person);
        double rx = Math.random()*getWidth();
        double ry = Math.random()*getHeight();
        //llamar x, y en person y usarlos
        person.updateStateRandom(rx, ry);
    } //Asigna la persona a la comuna y la coloca en una coordenada x-y aleatoria dentro de los rangos permitidos.
    public void setPeople(double speed, double deltaAngle){
        if(!(people.isEmpty())){
            clearPastViews();
        }
        people = new ArrayList<Pedestrian>();
        for(int i=0; i<NumberOfPeople;i++){
            if (i<NumberOfInfected){
                Pedestrian person = new Pedestrian(this, speed, deltaAngle, "I");
                this.setPerson(person);
            }else{
                Pedestrian person = new Pedestrian(this, speed, deltaAngle, "S");
                this.setPerson(person);
            }
        }
    }//Se utiliza para poner gente en el territorio con nuevas variables al utilizar start() de simulador.
    public void setMaskPeople(double M) { //Coloca mascarillas hasta que sea necesario indicado por el double M.
        int cant = (int)(people.size() * M);
        for (int i = 0; i < cant; i++) {
            int rand = (int)(Math.random() * people.size());
            if (people.get(rand).getMask() == "ON") {
                i--;
            } else {
                people.get(rand).setMask();
            }
        }
    }
    public void setVacs(double timeActVac, double simtime){
        if (vacunatorios.isEmpty()){
            if (simtime>timeActVac){
                for(int i=0; i<NumberOfVacs; i++) {
                        Vacunatorio vacunatorio = new Vacunatorio(SizeOfVacs, this);
                        if (i>0) {
                            for (int j = 0; j < i; j++) {
                                while (vacunatorios.get(j).overlap(vacunatorio)){
                                    vacunatorio.setAnotherLoc();
                                }
                            }
                        }
                        VacunatorioView vacview = new VacunatorioView(this, vacunatorio, SizeOfVacs);
                        vacview.setLocSucursal(this);
                        vacunatorios.add(vacunatorio);
                }
            }
        }
    }
    public void setGraphView(double simtime){
        if (graph.getData().isEmpty()){ //Si el grafico esta vacio, entonces crea nuevas series y arreglas los axis para que parta la simulacion de nuevo.
            sS = new XYChart.Series<Number, Number>();
            sI = new XYChart.Series<Number, Number>();
            sR = new XYChart.Series<Number, Number>();
            sV = new XYChart.Series<Number, Number>();
            sS.setName("Susceptibles");
            sI.setName("Infected");
            sR.setName("Recovered");
            sV.setName("Vaccinated");
            graph.getData().addAll(sV, sI, sR, sS);
            graph.getStylesheets().add("Chart.css");
            countAxis.setUpperBound(NumberOfPeople);
            timeAxis.setLowerBound(simtime);
            timeAxis.setUpperBound(simtime+100);
        }
    }
    public void setGraph(XYChart gr, XYChart.Series v, XYChart.Series s, XYChart.Series i, XYChart.Series r, NumberAxis cA, NumberAxis cT){
        graph = gr;
        sS = s;
        sI = i;
        sR = r;
        sV = v;
        countAxis = cA;
        timeAxis = cT;
    }
    public void setNumberOfPeople(double np){
        NumberOfPeople = np;
    } //Numero utilizado para indicarle a la comuna cuante gente se creara cuando la simulacion ocurre start.
    public void setNumberOfInfected(double ni){
        NumberOfInfected = ni;
    } //Numero utilizado para indicarle a la comuna cuante gente infectada se creara cuando la simulacion ocurre start.
    public void setNumberOfVacs(double nv){
        NumberOfVacs = nv;
    }
    public void setSizeOfVacs(double sv){
        SizeOfVacs = sv;
    }
    public ArrayList<Vacunatorio> getVacs() {
        return vacunatorios;
    }
    public void eraseVacs(){
        if (!(vacunatorios.isEmpty())){
            for (int i = 0; i < vacunatorios.size(); i++){
                vacunatorios.get(i).getView().erase();
            }
            vacunatorios = new ArrayList<Vacunatorio>();
        }
    }
}
