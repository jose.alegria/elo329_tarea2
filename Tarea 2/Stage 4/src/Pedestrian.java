import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class Pedestrian {
    private double x, y, speed, angle, deltaAngle;
    private Point2D ubicacion;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private PedestrianView view; //Stage 1 en adelante fue necesario establecer un view para las personas para poder presentarlas de manera grafica.
    private String estadoSalud;
    private String estadoSalud_tPlusDelta;
    private String mask;
    private double timerRecuperacion;
    static private Media media = new Media("https://www.myinstants.com/media/sounds/movie_1.mp3"); //Es el mediaplayer que las personas ocuparan para alertar de una contagio.
    static private MediaPlayer mediaplayer = new MediaPlayer(media);
    static public int audioToggle = 1;

    // Constructor
    public Pedestrian(Comuna comuna, double speed, double deltaAngle, String salud) {
        x = 0;
        y = 0;
        angle = Math.random() * 2 * Math.PI; // Le asigna un angulo de direccion aleatorio a la persona entre 0-2PI rad.
        this.comuna = comuna;
        this.speed = speed;
        this.deltaAngle = deltaAngle;
        ubicacion = new Point2D.Double(x,y);
        estadoSalud = salud;
        estadoSalud_tPlusDelta = salud;
        timerRecuperacion = 0;
        mask = "OFF";
    }

    // Get Methods
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public String getMask(){return mask;}
    public String getState(){
        return estadoSalud;
    }

    // State Methods
    public void computeAngle(){
        double r = Math.random(); // r entre 0 y 1
        double r2 = Math.random(); // r entre 0 y 1 para ver naturaleza del cambio de angulo
        if (r2 >= 0.5) {
            angle += r * deltaAngle; // cambia el angulo con delta positivo
        } else {
            angle -= r * deltaAngle; // cambia el angulo con delta negativo
        }
    } //Calculo del prox angulo en delta_t
    public void computeXY(double delta_t){
        double time;
        double time2;
        double angle2 = angle;

        if ((x + speed * delta_t * Math.cos(angle)) < 0) {
            time = x / (speed * (Math.abs(Math.cos(angle))));
            time2 = delta_t - time;
            x_tPlusDelta = x + speed * time * (Math.cos(angle));
            angle = (3 * Math.PI) - angle;
            x_tPlusDelta = x_tPlusDelta + speed * time2 * (Math.cos(angle));
        } else if ((x + speed * delta_t * Math.cos(angle)) > comuna.getWidth()) {
            time = (comuna.getWidth() - x) / (speed * (Math.abs(Math.cos(angle))));
            time2 = delta_t - time;
            x_tPlusDelta = x + speed * time * (Math.cos(angle));
            angle = (3 * Math.PI) - angle;
            x_tPlusDelta = x_tPlusDelta + speed * time2 * (Math.cos(angle));
        } else {
            x_tPlusDelta = x + speed * delta_t * Math.cos(angle);
        }

        if ((y + speed * delta_t * Math.sin(angle2)) < comuna.getHeight()
                && (y + speed * delta_t * Math.sin(angle2)) > 0) {
            y_tPlusDelta = y + speed * delta_t * Math.sin(angle2);

        } else if (y + speed * delta_t * Math.sin(angle2) > comuna.getHeight()) {

            time = (comuna.getHeight() - y) / (speed * (Math.abs(Math.sin(angle2))));
            time2 = delta_t - time;
            y_tPlusDelta = y + speed * time * Math.sin(angle2);
            angle2 = (2 * Math.PI) - angle2;
            y_tPlusDelta = y_tPlusDelta + (speed * time2 * (Math.sin(angle2)));
            angle = angle2;

        } else if (y + speed * delta_t * Math.sin(angle2) < 0) {
            time = y / (speed * (Math.abs(Math.sin(angle2))));
            time2 = delta_t - time;
            y_tPlusDelta = y + speed * time * Math.sin(angle2);
            angle2 = (2 * Math.PI) - angle2;
            y_tPlusDelta = y_tPlusDelta + (speed * time2 * ((Math.sin(angle2))));
            angle = angle2;
        }
    } //Calculo de x_tPlusDelta y y_tPlusDelta
    public void computeNextState(double delta_t,double d, double p0, double p1, double p2, double tiempoInfeccion) {
        if (estadoSalud == "S") {
            LookForVacuna();
        } // Solo si es susceptible revisara por vacunatorios, ya que son los unicos que se pueden vacunar para pasar a "V"
        if (estadoSalud == "S" && estadoSalud_tPlusDelta != "V"){
            LookforPersonInf(comuna.getPeople(), d, p0, p1, p2);
        } else if (estadoSalud == "I") {
            startTimerRecovery(delta_t, tiempoInfeccion);
        }
        computeAngle();
        computeXY(delta_t);
    }
    //Se preocupa de actualizar variables como estadoSalud_tPlusDelta, x_tPlusdeta y y_tPlusdeta.
    public void LookforPersonInf(ArrayList<Pedestrian> close_person, double d, double p0, double p1, double p2){
        for(int i=0; i< close_person.size(); i++){
            double D = Math.sqrt((x-(close_person.get(i).getX()))*(x-(close_person.get(i).getX())) + (y-(close_person.get(i).getY()))*(y-(close_person.get(i).getY())));
            if (D <= d && estadoSalud == "S" && close_person.get(i).getState() == "I"){
                if (mask == "ON" && close_person.get(i).getMask() == "ON"){
                    double r = Math.random();
                    //Desde el Stage 2 en adelante, cuando la gente se enferma utilizamos el media player para "alertar" al usuario
                    // con una notificacion de audio, este media player es compartido por todas las persona y antes de que se reproduzca siempre busca estar al principio del audio con seek.
                    if (r <= p2) {
                        if (audioToggle == 1) { //Solo si el boton de audio esta ON se repoducira el sonido establecido.
                            mediaplayer.seek(mediaplayer.getStartTime());
                            mediaplayer.play();
                        }
                        estadoSalud_tPlusDelta= "I";
                    }
                }else if((mask == "ON" && close_person.get(i).getMask() == "OFF") ||
                        (mask == "OFF" && close_person.get(i).getMask() == "ON")){
                    double r = Math.random();
                    if (r <= p1) {
                        if (audioToggle == 1) {
                            mediaplayer.seek(mediaplayer.getStartTime());
                            mediaplayer.play();
                        }
                        estadoSalud_tPlusDelta= "I";
                    }
                }else if(mask == "OFF" && close_person.get(i).getMask() == "OFF"){
                    double r = Math.random();
                    if (r <= p0) {
                        if (audioToggle == 1) {
                            mediaplayer.seek(mediaplayer.getStartTime());
                            mediaplayer.play();
                        }
                        estadoSalud_tPlusDelta= "I";
                    }
                }
            }
        }//Calcula la distancia entre el individuo y cada uno de los otros que se encuentran en la comuna, y una vez que encuentra
        //uno lo suficientemente cerca, revisa  segun las probabilidades si es que se infectara.
    }

    public void LookForVacuna(){
        for (int i = 0; i < comuna.getVacs().size(); i++) {
            if (comuna.getVacs().get(i).getVacunatory().contains(x,y) && estadoSalud == "S"){
                estadoSalud_tPlusDelta = "V";
            } //Llamamos cada vacunatorio del array de la comuna, para revisar a traves del metodo contain(x,y), para ver si la persona se encuentra dentro de un vacunatorio.
        }
    }//Revisa si se encontrara pasando por un vacunatorio en su proximo estado, si es asi se recuperara.

    public void startTimerRecovery(double delta_t, double tiempoInfeccion){
        timerRecuperacion += delta_t;
        if(timerRecuperacion>=tiempoInfeccion){
            estadoSalud_tPlusDelta = "R";
            timerRecuperacion = 0;
        } //Ocupa su propio timer para ver si suficiente tiempo ocurrio para que el nuevo estadoSalud sea "R")
    }

    //Update Methods
    public void updateState() {
        x = x_tPlusDelta;
        y = y_tPlusDelta;
        ubicacion.setLocation(x_tPlusDelta, y_tPlusDelta);
        estadoSalud = estadoSalud_tPlusDelta;
    } //Va actualizando posiciones
    public void updateStateRandom(double x, double y) {
        this.x = x;
        this.y = y;
        view = new PedestrianView(comuna,this,estadoSalud); //Se crea el view correspondiente a la persona una vez que esta se ubica en una parte aleatoria de la comuna.
    } //Solo es utilizado cuando la comuna busca colocar al individuo en un lugar random de su territorio.
    public void updateView(){
        view.update();
    }
    public void eraseView(){
        view.clearView();
    }
    public void setMask(){mask ="ON";}
}
