import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;

public class PedestrianView {
    private Pedestrian person;  //Se incluye la persona como uno de los atributos en el view para poder acceder a sus posiciones de manera mas comoda.
    private Rectangle viewR;   //Para presentar la ubicacion de la persona, ocupamos shapes: rectangles, circle y polygons (para los triangulos).
    private Circle viewC;      //Las tres shapes siempre van a estar "siguiendo" al peaton, pero solo una sera visible para mostar sus estado de salud.
    private Polygon viewT;

    private final double SIZE = 5;

    public PedestrianView(Comuna c, Pedestrian p, String salud) {
        person = p;
        double x = person.getX();
        double y = person.getY();
        if (salud == "S") { //El view es creado con el cuadrado azul visible
            viewT = new Polygon(x-SIZE, y-SIZE, x+SIZE, y-SIZE, x, y + SIZE);
            viewT.setFill(Color.TRANSPARENT);
            viewC = new Circle(x,y,SIZE,Color.TRANSPARENT);
            viewR = new Rectangle(SIZE*2, SIZE*2, Color.BLUE);
            viewR.setX(x - SIZE / 2);   // Rectangle x position is the X coordinate of the
            // upper-left corner of the rectangle
            viewR.setY(y - SIZE / 2); // Rectangle y position is the Y coordinate of the
            // upper-left corner of the rectangle
            c.getView().getChildren().addAll(viewT,viewR,viewC); //Se asegura de agregar las vistas a la view de comuna.
        } else if(salud == "I"){ //El view es creado con el circulo rojo visible
            viewC = new Circle(x, y, SIZE, Color.RED);
            viewR = new Rectangle(SIZE*2, SIZE*2, Color.TRANSPARENT);
            viewT = new Polygon(x-SIZE, y-SIZE, x+SIZE, y-SIZE, x, y + SIZE);
            viewT.setFill(Color.TRANSPARENT);
            viewR.setX(x - SIZE / 2);   // Rectangle x position is the X coordinate of the
            // upper-left corner of the rectangle
            viewR.setY(y - SIZE / 2); // Rectangle y position is the Y coordinate of the
            // upper-left corner of the rectangle
            c.getView().getChildren().addAll(viewT,viewR,viewC); //Se asegura de agregar las vistas a la view de comuna.
        }
    }

    public void clearView(){            //De la manera que decidimos limpiar la vista para luego empezar la simulacion con otro grupo,
        viewC.setFill(Color.TRANSPARENT);// fue asignar color transparente a todas las vistas correspondientes, de tal manera que no
        viewR.setFill(Color.TRANSPARENT); // obstruyeran la vistas nuevas.
        viewT.setFill(Color.TRANSPARENT);
        viewC.setStroke(Color.TRANSPARENT);
        viewR.setStroke(Color.TRANSPARENT);
        viewT.setStroke(Color.TRANSPARENT);
        viewC.setRadius(0);
        viewR.setWidth(0);
        viewR.setHeight(0);
    }
    public void update() {
        double x = person.getX(); //La actualizacion de vistas es realizada a traves de las coordenadas de cada persona y los metodos correspondientes
        double y = person.getY(); // de cada shape. setX,setY para rectangle y circle. y un getPoints.setAll para el triangulo.
        viewR.setStrokeWidth(3.0);
        viewC.setStrokeWidth(3.0);
        viewR.setX(person.getX() - SIZE / 2);
        viewR.setY(person.getY() - SIZE / 2);
        viewC.setCenterX(person.getX());
        viewC.setCenterY(person.getY());
        viewT.getPoints().setAll(x-SIZE, y-SIZE, x+SIZE, y-SIZE, x, y + SIZE);
        if (person.getMask() =="ON" && person.getState() == "S"){ //Revisa si la persona tiene mascarilla, si ese el caso, se le pone un borde negro para demostrarlo.
            viewR.setStroke(Color.BLACK);
        }
        else if (person.getState() == "I") {
            viewR.setFill(Color.TRANSPARENT);
            viewC.setFill(Color.RED);
            if(person.getMask() == "ON"){ //Tambien se revisa si la persona tiene mascarilla, pero en el caso de los ya infectados.
                viewR.setStroke(Color.TRANSPARENT);
                viewC.setStroke(Color.BLACK);
            }
        }else if (person.getState() == "R"){
            viewR.setStroke(Color.TRANSPARENT);
            viewC.setStroke(Color.TRANSPARENT);
            viewR.setFill(Color.BROWN);
            viewC.setFill(Color.TRANSPARENT);
            viewT.setFill(Color.TRANSPARENT);
        }else if (person.getState() == "V"){
            viewR.setStroke(Color.TRANSPARENT);
            viewC.setStroke(Color.TRANSPARENT);
            viewR.setFill(Color.TRANSPARENT);
            viewC.setFill(Color.TRANSPARENT);
            viewT.setFill(Color.GREEN);
        }
    }
}
