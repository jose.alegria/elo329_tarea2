import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.chart.XYChart;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;

public class Simulator {
    private Timeline animation;
    private Comuna comuna;
    private double simulationSamplingTime;
    private double simulationTime;  // acompaña el tiempo real, más rapido o más lento.
    private double speed;
    private double delta_t;   // precision del tiempo de la simulacion.
    private double delta_theta;
    private double dist;
    private double masks;
    private double prob0;
    private double prob1;
    private double prob2;
    private double tInfeccion;
    private double timevacs;
    private double viewRefreshPeriod;
    /**
     * @param framePerSecond frequency of new views on screen
     * @param simulationTime2realTimeRate how faster the simulation runs relative to real time
     * @param comuna la comuna :)
     */
    public Simulator (double framePerSecond, double simulationTime2realTimeRate, Comuna comuna){
        this.comuna = comuna;
        double viewRefreshPeriod = 1 / framePerSecond; // in [ms] real time used to display
        // a new view on application
        simulationSamplingTime = viewRefreshPeriod * simulationTime2realTimeRate;
        delta_t = SimulatorConfig.DELTA_T;              //De la linea 37 a la 45, nos aseguramos de guardar la informacion del archivo de entrada
        speed = SimulatorConfig.SPEED;                  //en sus variables para luego poder modificarlas y acceder a ellas de distintas maneras.
        delta_theta = SimulatorConfig.DELTA_THETA;
        dist = SimulatorConfig.D;
        masks = SimulatorConfig.M;
        prob0 = SimulatorConfig.P0;
        prob1 = SimulatorConfig.P1;
        prob2 = SimulatorConfig.P2;
        tInfeccion = SimulatorConfig.I_TIME;
        timevacs = SimulatorConfig.VAC_TIME;
        simulationTime = 0;
        animation = new Timeline(new KeyFrame(Duration.millis(viewRefreshPeriod*1000), e->takeAction()));
        animation.setCycleCount(Timeline.INDEFINITE);
    }
    private void takeAction() {
        double nextStop=simulationTime+simulationSamplingTime;
        for(; simulationTime<nextStop; simulationTime+=delta_t) {
            comuna.computeNextState(delta_t,dist,prob0,prob1,prob2,tInfeccion); // compute its next state based on current global state
            comuna.updateState();
            comuna.updateHealthState();
            comuna.updateView(simulationTime);// Actualiza el estado visual solo una vez que el resto de la informacion haya sido actualizada.
            comuna.setVacs(timevacs,simulationTime);
        }
    }
    public void start(){
        simulationTime = 0;
        comuna.getView().getChildren().clear();
        comuna.getView().territoryRefresh();
        comuna.eraseVacs();//Cada vez que se parte, se asegura que la simulacion parta del segundo 0.
        comuna.setPeople(speed,delta_theta); //Se crean las personas indicadas por interfaz grafica.
        comuna.setMaskPeople(masks); //Coloca mascarillas
        comuna.setGraphView(simulationTime); //Remueve vistas pasadas y coloca nueva vista grafica para el grafico.
        animation.playFromStart(); //Parte la animacion.
        comuna.getView().setOnKeyPressed( e->keyHandle(e));
    }
    private void keyHandle (KeyEvent e) {
        if (e.getCode() == KeyCode.RIGHT){ //Speedup
            simulationSamplingTime *=2; //Esto aumenta la velocidad de simulacion en 2
        }else if(e.getCode() == KeyCode.LEFT){//Slowdown
            simulationSamplingTime *=0.5; //Esto disminuye la velocidad de la simulacion a la mitad.
        }
    }
    public void stop(){
        animation.stop();
    }
    //Setter & Get methods
    public void setSpeed(double s){
        speed = s;
    }
    public void setDelta_t(double dt){
        delta_t = dt;
    }
    public void setDelta_theta(double dt){
        delta_theta = dt;
    }
    public void setDist(double d){
        dist = d;
    }
    public void setMasks(double m){
        masks = m;
    }
    public void setProb0(double p0){
        prob0 = p0;
    }
    public void setProb1(double p1){
        prob1 = p1;
    }
    public void setProb2(double p2){
        prob2 = p2;
    }
    public void settInfeccion(double ti){
        tInfeccion = ti;
    }
    public void setTimevacs(double tv){ timevacs = tv;}
    public Comuna getComuna(){
        return comuna;
    }
}
