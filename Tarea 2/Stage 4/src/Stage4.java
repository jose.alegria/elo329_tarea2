import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedAreaChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import java.io.File;
import java.util.List;
import java.util.Scanner;

public class Stage4 extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parameters param = getParameters();
        List<String> rawParam = param.getRaw();
        if (rawParam.size() != 1) {
            System.out.println("Usage: java Stage1 <configurationFile.txt>");
            System.exit(-1);
        }
        primaryStage.setTitle("Pandemic Graphics Simulator");
        BorderPane borderPane = new BorderPane();
        primaryStage.setScene(new Scene(borderPane, 600, 400));
        SimulatorConfig config = new SimulatorConfig(new Scanner(new File(rawParam.get(0))));
        Comuna comuna = new Comuna();
        Simulator simulator = new Simulator(10,1,comuna);
        borderPane.setTop(new SimulatorMenuBar(simulator));
        SplitPane splitPane = new SplitPane();
        splitPane.setOrientation(Orientation.VERTICAL);
        borderPane.setCenter(splitPane);
        StackPane pane = new StackPane();
        //Se crea el grafico desde un principio para el primer grupo de personas que se forma con la info. del archivo de entrada.
        NumberAxis xAxis = new NumberAxis(0,100,1);
        xAxis.setForceZeroInRange(false);
        NumberAxis yAxis = new NumberAxis(1,(int)SimulatorConfig.N,1);
        StackedAreaChart<Number,Number> peopleGraph = new StackedAreaChart<Number,Number>(xAxis, yAxis);
        peopleGraph.setTitle("People's Health");
        XYChart.Series<Number,Number> seriesSusceptibles = new XYChart.Series<Number,Number>();
        XYChart.Series<Number,Number> seriesInfected = new XYChart.Series<Number,Number>();
        XYChart.Series<Number,Number> seriesRecovered = new XYChart.Series<Number,Number>();
        XYChart.Series<Number,Number> seriesVaccinated = new XYChart.Series<Number,Number>();
        seriesSusceptibles.setName("Susceptibles");
        seriesInfected.setName("Infected");
        seriesRecovered.setName("Recovered");
        seriesVaccinated.setName("Vaccinated");
        peopleGraph.getData().addAll(seriesVaccinated, seriesInfected,seriesRecovered,seriesSusceptibles);
        peopleGraph.getStylesheets().add("Chart.css");
        StackPane comunaGraph = new StackPane();
        comunaGraph.getChildren().add(peopleGraph);
        comuna.setGraph(peopleGraph, seriesVaccinated, seriesSusceptibles, seriesInfected, seriesRecovered,yAxis,xAxis);

        pane.getChildren().add(comuna.getView());
        splitPane.getItems().addAll(pane, comunaGraph); //Se agrega la vista del comuna, y el grafico inicial.
        pane.setAlignment(comuna.getView(), Pos.CENTER);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
