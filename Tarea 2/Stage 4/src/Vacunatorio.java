import java.awt.geom.Rectangle2D;


public class Vacunatorio{
    private Rectangle2D AreaVac;
    private Comuna comuna;
    private VacunatorioView view;
    private double size;
    //Creamos este objeto el cual es un cuadrado llamado vacunatorio, por donde la gente
    // con estado de salud S, puede vacunarse pasando a "V" trabaja en conjunto a Point2D de java.
    public Vacunatorio(double size, Comuna lugar){
        comuna = lugar;
        this.size = size;
         //Al momento de iniciarlizar cada vacunatorio nos aseguramos de que no pudieran salirse de los bordes del territorio.
        AreaVac = new Rectangle2D.Double(Math.random()*(comuna.getWidth()-size), Math.random()*(comuna.getHeight()-size), size, size);
        view = new VacunatorioView(comuna,this, size);
    }
    public void setAnotherLoc(){ //Utilizada en el caso de que el vacunatorio tope con otros.
        AreaVac.setRect(Math.random()*(comuna.getWidth()-size), Math.random()*(comuna.getHeight()-size), size, size);
    }
    public boolean overlap(Vacunatorio r){ //Utilziado para verificar que vacunatorios no se topen.
       return AreaVac.getX() < r.getX() + r.getWidth() && AreaVac.getX() + AreaVac.getWidth() > r.getX() && AreaVac.getY() < r.getY() + r.getHeight() && AreaVac.getY() + AreaVac.getHeight() > r.getY();
    }
    public Rectangle2D getVacunatory(){
        return AreaVac;
    } //Retorna el area que aborda el vacunatorio en un sistema x-y
    public VacunatorioView getView(){
        return view;
    }
    public double getX(){
        return AreaVac.getX();
    }
    public double getY(){
        return AreaVac.getY();
    }
    public double getWidth(){
        return AreaVac.getWidth();
    }
    public double getHeight(){
        return AreaVac.getHeight();
    }
}
