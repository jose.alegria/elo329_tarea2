import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;

public class VacunatorioView {
    private Rectangle sucursal;
    private Vacunatorio vac;

    public VacunatorioView(Comuna c, Vacunatorio va, double size){
        vac = va;
        sucursal = new Rectangle(size, size, Color.rgb(60, 179, 113, 0.5));
        sucursal.setX(vac.getX());
        sucursal.setY(vac.getY());
    }
    public void erase(){
        sucursal.setFill(Color.TRANSPARENT);
        sucursal.setY(0);
        sucursal.setX(0);
        sucursal.setWidth(0);
        sucursal.setHeight(0);
    }
    public void setLocSucursal(Comuna comuna){
        comuna.getView().getChildren().addAll(sucursal);;
    }
}
